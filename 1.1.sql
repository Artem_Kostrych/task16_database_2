select maker
from product
where maker in (SELECT maker 
from product
where model IN (SELECT model
from laptop)) and maker not in (SELECT maker
from product
where model IN (SELECT model
from pc));