CREATE DATABASE University22;
USE University22 ;
CREATE TABLE Student
(	ID 				int 	AUTO_INCREMENT 	PRIMARY KEY,
	name 			char(20)NULL,
	surname			char(20), 
    image			varchar(50),
    biography 		char(200),
    group_code		int		NULL,
    profession_code		int	NULL,
    entry_year		date,
    bith_year		date,
    address			varchar(50),
    rating 			int,
    scholarship_code	int	NULL,
	subject_code	int 	NULL,
	first_module	int,
    second_module	int,
	mark			int
);

CREATE TABLE Subject
(
	id 		int 	AUTO_INCREMENT 	PRIMARY KEY,
    name 	char(20),
    teacher char(20),
    semester_number int
);



CREATE TABLE Scholarship
(
	id 		int 	AUTO_INCREMENT 	PRIMARY KEY,
    type 	char(20),
    amount  int
    
    
);

CREATE TABLE Profession
(
	id 		int 	AUTO_INCREMENT 	PRIMARY KEY,
    name 	char(20)
);

CREATE TABLE Group1
(
	id 			int 	AUTO_INCREMENT 	PRIMARY KEY,
    name 		char(20),
    institute 	char(20),
    amount_of_student  int
);

ALTER TABLE Student ADD CONSTRAINT FK_Students_Group1 FOREIGN KEY (group_code) REFERENCES Group1(id);
ALTER TABLE Student ADD CONSTRAINT FK_Students_Profession FOREIGN KEY (profession_code) REFERENCES Profession(id);
ALTER TABLE Student ADD CONSTRAINT FK_Students_Scholarship FOREIGN KEY (scholarship_code) REFERENCES Scholarship(id);